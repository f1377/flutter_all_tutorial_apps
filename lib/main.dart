import 'package:apps_collection/apps/bmi_calculator/bmi_calculator_screen.dart';
import 'package:apps_collection/apps/destini/destini_screen.dart';
import 'package:apps_collection/apps/dicee/dicee_screen.dart';
import 'package:apps_collection/apps/home_screen.dart';
import 'package:apps_collection/apps/i_am_poor/i_am_poor_screen.dart';
import 'package:apps_collection/apps/i_am_rich/i_am_rich_screen.dart';
import 'package:apps_collection/apps/magic_8_ball/magic_8_ball_screen.dart';
import 'package:apps_collection/apps/my_card/my_card_screen.dart';
import 'package:apps_collection/apps/quizzler/quizzler_screen.dart';
import 'package:apps_collection/apps/viktonius/viktonius_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      routes: {
        '/': (context) => HomeScreen(),
        'iAmRich': (context) => IAmRichScreen(),
        'iAmPoor': (context) => IAmPoorScreen(),
        'myCard': (context) => MyCardScreen(),
        'dicee': (context) => DiceeScreen(),
        'magic8Ball': (context) => Magic8BallScreen(),
        'viktonius': (context) => ViktoniusScreen(),
        'quizzler': (context) => QuizzlerScreen(),
        'destini': (context) => DestiniScreen(),
        'bmiCalculator': (context) => BMICalculatorScreen(),
      },
    );
  }
}
