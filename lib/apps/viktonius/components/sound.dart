class Sound {
  Sound({
    required this.name,
    required this.path,
    required this.imgPath,
  });

  final String name;
  final String path;
  final String imgPath;
}
