import 'package:apps_collection/apps/viktonius/constants.dart';
import 'package:flutter/material.dart';

class SoundButton extends StatelessWidget {
  SoundButton({
    required this.onTap,
    required this.imgPath,
    required this.soundName,
  });

  final void Function() onTap;
  final String imgPath;
  final String soundName;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(4),
                  topLeft: Radius.circular(4),
                ),
                color: kButtonBackgroundColor,
              ),
              child: Image.asset(
                imgPath,
                width: kButtonImageWidth,
                height: kButtonImageHeight,
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(4),
                bottomRight: Radius.circular(4),
              ),
              color: Colors.blueGrey.shade600,
            ),
            child: Center(
              child: Text(
                soundName,
                style: kButtonTextStyle,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
