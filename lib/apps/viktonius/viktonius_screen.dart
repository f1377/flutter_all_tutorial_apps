import 'package:apps_collection/apps/viktonius/components/sound.dart';
import 'package:apps_collection/apps/viktonius/components/sound_button.dart';
import 'package:apps_collection/apps/viktonius/components/sound_data.dart';
import 'package:apps_collection/apps/viktonius/constants.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

SoundData soundData = SoundData();

class ViktoniusScreen extends StatefulWidget {
  @override
  _ViktoniusScreenState createState() => _ViktoniusScreenState();
}

class _ViktoniusScreenState extends State<ViktoniusScreen> {
  final AudioCache _player = AudioCache();
  List<Widget> gridChildren = [];

  @override
  void initState() {
    super.initState();
    setGridChildren();
  }

  void setGridChildren() {
    List<Sound> sounds = soundData.getSoundData();

    for (int soundIndex = 0; soundIndex < sounds.length; soundIndex++) {
      String soundName = sounds[soundIndex].name;
      String soundImg = sounds[soundIndex].imgPath;

      String soundPath = sounds[soundIndex].path;
      void Function() ps = () {
        playSoundFromPath(soundPath);
      };

      gridChildren.add(SoundButton(
        soundName: soundName,
        onTap: ps,
        imgPath: soundImg,
      ));
    }
  }

  void playSoundFromPath(String path) {
    _player.play(path);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackgroundColor,
      appBar: AppBar(
        title: Center(
          child: Text(
            '$kTitle'.toUpperCase(),
          ),
        ),
      ),
      body: SafeArea(
        child: GridView.count(
          crossAxisSpacing: 4,
          mainAxisSpacing: 4,
          padding: EdgeInsets.all(2),
          crossAxisCount: 3,
          children: gridChildren,
        ),
      ),
    );
  }
}
