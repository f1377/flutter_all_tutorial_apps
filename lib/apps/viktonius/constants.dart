import 'package:flutter/material.dart';

const String kBaseSoundPath = 'sounds/viktonius';
const String kBaseImagePath = 'images/viktonius';

/* Screen */
const String kTitle = 'viktonius';
const Color kBackgroundColor = Colors.black;

/* Sound Button */
const Color kButtonBackgroundColor = Colors.blueGrey;
const double kButtonImageWidth = 100;
const double kButtonImageHeight = 100;
const Color kButtonBorderColor = Colors.blueGrey;
const TextStyle kButtonTextStyle = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 12,
  color: Colors.white,
);
