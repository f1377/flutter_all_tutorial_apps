import 'package:flutter/material.dart';

class IAmRichScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey.shade900,
        title: Center(
          child: Text(
            'i am rich'.toUpperCase(),
          ),
        ),
      ),
      body: Center(
        child: Image(
          image: AssetImage('images/i_am_rich/diamond.png'),
        ),
      ),
    );
  }
}
