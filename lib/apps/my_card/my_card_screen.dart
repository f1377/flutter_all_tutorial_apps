import 'package:flutter/material.dart';

class MyCardScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(
            'my card'.toUpperCase(),
          ),
        ),
      ),
      backgroundColor: Colors.teal,
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 120.0,
              width: 100.0,
              child: CircleAvatar(
                radius: 50.0,
                backgroundImage: AssetImage('images/my_card/triss.jpg'),
              ),
            ),
            Text(
              'Triss Merigold',
              style: TextStyle(
                fontSize: 40.0,
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontFamily: 'Pacifico',
              ),
            ),
            Text(
              'Sorceress'.toUpperCase(),
              style: TextStyle(
                fontSize: 20.0,
                fontFamily: 'Source Sans Pro',
                fontWeight: FontWeight.bold,
                color: Colors.teal[100],
                letterSpacing: 2.5,
              ),
            ),
            Card(
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
              child: ListTile(
                leading: Icon(
                  Icons.all_inclusive,
                  color: Colors.teal,
                ),
                title: Text(
                  'Fireballs',
                  style: TextStyle(
                    color: Colors.teal.shade900,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
              child: ListTile(
                leading: Icon(
                  Icons.favorite,
                  color: Colors.teal,
                ),
                title: Text(
                  'Geralt of Riva',
                  style: TextStyle(
                    color: Colors.teal.shade900,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
