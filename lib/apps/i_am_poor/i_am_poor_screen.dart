import 'package:flutter/material.dart';

class IAmPoorScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey.shade900,
        title: Center(
          child: Text(
            'i am poor'.toUpperCase(),
          ),
        ),
      ),
      body: Center(
        child: Image.asset('images/i_am_poor/coal.png'),
      ),
    );
  }
}
