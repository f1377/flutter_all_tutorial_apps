import 'package:apps_collection/apps/quizzler/components/quiz_page.dart';
import 'package:flutter/material.dart';

class QuizzlerScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade900,
      appBar: AppBar(
        title: Center(
          child: Text(
            'quizzler'.toUpperCase(),
          ),
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          child: QuizPage(),
        ),
      ),
    );
  }
}
