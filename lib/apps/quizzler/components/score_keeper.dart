import 'package:flutter/material.dart';

class ScoreKeeper {
  List<Icon> _scoreKeeper = [];

  Icon _trueIcon = Icon(
    Icons.check,
    color: Colors.green,
  );

  Icon _falseIcon = Icon(
    Icons.clear,
    color: Colors.red,
  );

  void addTrueToScore() {
    _scoreKeeper.add(_trueIcon);
  }

  void addFalseToScore() {
    _scoreKeeper.add(_falseIcon);
  }

  List<Icon> getScore() {
    return _scoreKeeper;
  }

  void resetScore() {
    _scoreKeeper = [];
  }
}
