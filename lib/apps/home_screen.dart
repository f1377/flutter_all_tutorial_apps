import 'package:apps_collection/widgets/app_button.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('apps collection'.toUpperCase())),
      ),
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xFF03DAc6),
                Color(0xFF018786),
              ],
            ),
          ),
          child: GridView.count(
            crossAxisCount: 3,
            children: [
              AppButton(route: 'iAmRich', title: 'I am Rich'),
              AppButton(route: 'iAmPoor', title: 'I am Poor'),
              AppButton(route: 'myCard', title: 'My Card'),
              AppButton(route: 'dicee', title: 'Dicetor'),
              AppButton(route: 'magic8Ball', title: 'Magic 8 Ball'),
              AppButton(route: 'viktonius', title: 'Viktonius'),
              AppButton(route: 'quizzler', title: 'Quizzler'),
              AppButton(route: 'destini', title: 'Destini'),
              AppButton(route: 'bmiCalculator', title: 'BMI Calculator'),
            ],
          ),
        ),
      ),
    );
  }
}
