import 'dart:math';

import 'package:flutter/material.dart';

class Magic8BallScreen extends StatefulWidget {
  @override
  _Magic8BallScreenState createState() => _Magic8BallScreenState();
}

class _Magic8BallScreenState extends State<Magic8BallScreen> {
  int _ballNumber = 1;

  void setRandomNumber() {
    setState(() {
      _ballNumber = Random().nextInt(5) + 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue.shade300,
      appBar: AppBar(
        title: Center(
          child: Text('magic 8 ball'.toUpperCase()),
        ),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GestureDetector(
            onTap: () {
              setRandomNumber();
            },
            child: Image.asset('images/magic_ball/ball$_ballNumber.png'),
          ),
        ),
      ),
    );
  }
}
