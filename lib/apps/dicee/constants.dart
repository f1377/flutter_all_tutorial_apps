import 'package:flutter/material.dart';

const String kAppBarTitle = 'dicetor';
const Color kScaffoldBackgroundColor = Color(0xFF33333D);

const String kSheetTitle = 'face sheet';
const TextStyle kSheetTitleStyle = TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.bold,
  fontSize: 40.0,
);

const Color kDividerColor = Colors.white;
const double kDividerThickness = 5;

const Color kButtonColor = Color(0xFF373740);
const BorderSide kButtonBorderSide = BorderSide(
  color: Colors.white,
  width: 5,
);
const TextStyle kButtonStyle = TextStyle(
  color: Colors.white,
  fontSize: 30.0,
  fontWeight: FontWeight.bold,
);

const TextStyle kDiceStyle = TextStyle(
  fontSize: 30.0,
  color: Colors.white,
  fontWeight: FontWeight.bold,
);
const double kDiceNumberPositionLeft = 40;
const double kDiceNumberPositionTop = 20;

const double kFaceSheetImgHeight = 80;
const double kFaceSheetImgWidth = 80;
const double kFaceSheetPositionLeft = 10;
const double kFaceSheetPositionTop = 10;
const TextStyle kFaceSheetStyle = TextStyle(
  color: Colors.white,
  fontSize: 18.0,
  fontWeight: FontWeight.bold,
);
