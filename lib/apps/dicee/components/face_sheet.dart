import 'package:apps_collection/apps/dicee/constants.dart';
import 'package:flutter/material.dart';

class FaceSheet extends StatelessWidget {
  FaceSheet({
    required this.title,
    required this.imgPath,
  });

  final String imgPath;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          child: Stack(
            children: [
              Image.asset(
                imgPath,
                height: kFaceSheetImgHeight,
                width: kFaceSheetImgWidth,
              ),
              Positioned(
                left: kFaceSheetPositionLeft,
                top: kFaceSheetPositionTop,
                child: Text(
                  title,
                  style: kFaceSheetStyle,
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
