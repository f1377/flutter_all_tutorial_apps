import 'package:apps_collection/apps/dicee/constants.dart';
import 'package:flutter/material.dart';

class Dice extends StatelessWidget {
  Dice({
    required this.onPressed,
    required this.diceNumber,
    required this.imgPath,
  });

  final void Function() onPressed;
  final String imgPath;
  final String diceNumber;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          TextButton(
            onPressed: onPressed,
            child: Image.asset(imgPath),
          ),
          Positioned(
            left: kDiceNumberPositionLeft,
            top: kDiceNumberPositionTop,
            child: Text(
              diceNumber,
              style: kDiceStyle,
            ),
          ),
        ],
      ),
    );
  }
}
