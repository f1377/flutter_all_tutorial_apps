import 'package:flutter/material.dart';

class ReusableCard extends StatelessWidget {
  final void Function() cardOnTap;
  final Color cardColor;
  final Widget cardChild;

  ReusableCard({
    required this.cardColor,
    required this.cardChild,
    this.cardOnTap: emptyFunction,
  });

  static void emptyFunction() {}

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: cardOnTap,
      child: Container(
        margin: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8.0),
          color: cardColor,
        ),
        child: cardChild,
      ),
    );
  }
}
