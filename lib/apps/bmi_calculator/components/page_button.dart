import 'package:apps_collection/apps/bmi_calculator/constants.dart';
import 'package:flutter/material.dart';

class PageButton extends StatelessWidget {
  PageButton({
    required this.onTap,
    required this.buttonTitle,
  });

  final void Function() onTap;
  final String buttonTitle;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        color: kBottomContainerColor,
        margin: EdgeInsets.only(top: 10.0),
        width: double.infinity,
        height: kBottomContainerHeight,
        child: Center(
          child: Text(
            buttonTitle,
            style: kLabelButtonStyle,
          ),
        ),
      ),
    );
  }
}
