import 'package:flutter/material.dart';

/* Resusable container const */
const double kBottomContainerHeight = 80.0;
const Color kActiveCardColor = Color(0xFF1D1E33);
const Color kInactiveCardColor = Color(0xFF111328);
const Color kBottomContainerColor = Color(0xFFEB1555);

/* Slider const */
const double kSliderMin = 100.0;
const double kSliderMax = 250.0;
const Color kSliderThumbColor = Color(0xFFEB1555);
const Color kSliderActiveTrackColor = Colors.white;
const Color kSliderInactiveTrackColor = Color(0xFF8D8E98);
const Color kSliderOverlayColor = Color(0x29EB1555);

/* Floating Button */
const Color kRoundIconButtonColor = Color(0xFF4C4F5E);
const int kMinWeight = 1;
const int kMaxWeight = 300;
const int kMinAge = 1;
const int kMaxAge = 130;

const TextStyle kLabelTextStyle = TextStyle(
  fontSize: 18.0,
  color: Color(0xFF8D8E98),
);

const TextStyle kLabelNumberStyle = TextStyle(
  fontSize: 50.0,
  fontWeight: FontWeight.w900,
  color: Colors.white,
);

const TextStyle kLabelButtonStyle = TextStyle(
  fontSize: 25.0,
  fontWeight: FontWeight.bold,
  color: Colors.white,
);

/* Result Page style */
const TextStyle kResultTitleStyle = TextStyle(
  fontSize: 50.0,
  fontWeight: FontWeight.bold,
  color: Colors.white,
);

const TextStyle kResultTextStyle = TextStyle(
  fontWeight: FontWeight.bold,
  color: Color(0xFF24d876),
  fontSize: 22.0,
);

const TextStyle kResultNumberStyle = TextStyle(
  fontSize: 100.0,
  fontWeight: FontWeight.bold,
  color: Colors.white,
);

const TextStyle kResultDescriptionStyle = TextStyle(
  fontSize: 22.0,
  color: Colors.white,
);
