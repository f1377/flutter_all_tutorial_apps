import 'package:apps_collection/apps/bmi_calculator/components/calculator_brain.dart';
import 'package:apps_collection/apps/bmi_calculator/components/icon_content.dart';
import 'package:apps_collection/apps/bmi_calculator/components/page_button.dart';
import 'package:apps_collection/apps/bmi_calculator/components/resuable_card.dart';
import 'package:apps_collection/apps/bmi_calculator/components/round_icon_button.dart';
import 'package:apps_collection/apps/bmi_calculator/constants.dart';
import 'package:apps_collection/apps/bmi_calculator/screens/calculation_page.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

enum Gender { MALE, FEMALE }

class BMICalculatorScreen extends StatefulWidget {
  @override
  _BMICalculatorScreenState createState() => _BMICalculatorScreenState();
}

class _BMICalculatorScreenState extends State<BMICalculatorScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: Center(
          child: Text(
            'bmi calculator'.toUpperCase(),
          ),
        ),
      ),
      body: PageStructure(),
    );
  }
}

class PageStructure extends StatefulWidget {
  @override
  _PageStructureState createState() => _PageStructureState();
}

class _PageStructureState extends State<PageStructure> {
  Color _maleCardColor = kActiveCardColor;
  Color _femaleCardColor = kInactiveCardColor;
  double _currentHeight = 160.0;
  int _currentWeight = 70;
  int _currentAge = 25;

  updateGenderCardColor(Gender gender) {
    _maleCardColor =
        gender == Gender.MALE ? kActiveCardColor : kInactiveCardColor;
    _femaleCardColor =
        gender == Gender.FEMALE ? kActiveCardColor : kInactiveCardColor;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Expanded(
          child: Row(
            children: [
              Expanded(
                child: ReusableCard(
                  cardOnTap: () {
                    setState(() {
                      updateGenderCardColor(Gender.MALE);
                    });
                  },
                  cardColor: _maleCardColor,
                  cardChild: IconContent(
                    icon: FontAwesomeIcons.mars,
                    label: 'male'.toUpperCase(),
                  ),
                ),
              ),
              Expanded(
                child: ReusableCard(
                  cardOnTap: () {
                    setState(() {
                      updateGenderCardColor(Gender.FEMALE);
                    });
                  },
                  cardColor: _femaleCardColor,
                  cardChild: IconContent(
                    icon: FontAwesomeIcons.venus,
                    label: 'female'.toUpperCase(),
                  ),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: ReusableCard(
            cardColor: kActiveCardColor,
            cardChild: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'height'.toUpperCase(),
                  style: kLabelTextStyle,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  textBaseline: TextBaseline.alphabetic,
                  children: [
                    Text(
                      _currentHeight.round().toString(),
                      style: kLabelNumberStyle,
                    ),
                    Text(
                      'cm',
                      style: kLabelTextStyle,
                    ),
                  ],
                ),
                SliderTheme(
                  data: SliderTheme.of(context).copyWith(
                    activeTrackColor: kSliderActiveTrackColor,
                    inactiveTrackColor: kSliderInactiveTrackColor,
                    thumbColor: kSliderThumbColor,
                    overlayColor: kSliderOverlayColor,
                    thumbShape: RoundSliderThumbShape(enabledThumbRadius: 15.0),
                    overlayShape: RoundSliderOverlayShape(overlayRadius: 30.0),
                  ),
                  child: Slider(
                    value: _currentHeight,
                    min: kSliderMin,
                    max: kSliderMax,
                    onChanged: (double value) {
                      setState(() {
                        _currentHeight = value;
                      });
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
        Expanded(
          child: Row(
            children: [
              Expanded(
                child: ReusableCard(
                  cardColor: kActiveCardColor,
                  cardChild: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'weight'.toUpperCase(),
                        style: kLabelTextStyle,
                      ),
                      Text(
                        _currentWeight.toString(),
                        style: kLabelNumberStyle,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          RoundIconButton(
                            icon: FontAwesomeIcons.minus,
                            onPressed: () {
                              setState(() {
                                if (_currentWeight > kMinWeight) {
                                  _currentWeight--;
                                }
                              });
                            },
                          ),
                          SizedBox(width: 10.0),
                          RoundIconButton(
                            icon: FontAwesomeIcons.plus,
                            onPressed: () {
                              setState(() {
                                if (_currentWeight < kMaxWeight) {
                                  _currentWeight++;
                                }
                              });
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: ReusableCard(
                  cardColor: kActiveCardColor,
                  cardChild: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'age'.toUpperCase(),
                        style: kLabelTextStyle,
                      ),
                      Text(
                        _currentAge.toString(),
                        style: kLabelNumberStyle,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          RoundIconButton(
                            icon: FontAwesomeIcons.minus,
                            onPressed: () {
                              setState(() {
                                if (_currentAge > kMinAge) {
                                  _currentAge--;
                                }
                              });
                            },
                          ),
                          SizedBox(width: 10.0),
                          RoundIconButton(
                            icon: FontAwesomeIcons.plus,
                            onPressed: () {
                              setState(() {
                                if (_currentAge < kMaxAge) {
                                  _currentAge++;
                                }
                              });
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        PageButton(
          onTap: () {
            CalculatorBrain calc = CalculatorBrain(
              weight: _currentWeight,
              height: _currentHeight.toInt(),
            );

            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) {
                return CalculationPage(
                  bmiResult: calc.calculateBMI(),
                  resultText: calc.getResultWord(),
                  resultInterpretation: calc.getInterpretation(),
                );
              }),
            );
          },
          buttonTitle: 'calculate'.toUpperCase(),
        ),
      ],
    );
  }
}
